#!/usr/bin/make -f
# debian/rules for libgtkada
# Copyright (c) 2003-2012 Ludovic Brenta <lbrenta@debian.org>
# Copyright (c) 2011-2022 Nicolas Boulenguez <nicolas@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# The full text of the GNU General Public License is in the file
# /usr/share/common-licenses/GPL-3 on Debian systems.

%:
	dh $@

include /usr/share/dpkg/architecture.mk

DPKG_EXPORT_BUILDFLAGS := 1

# -gnatVa triggers #998177.
DEB_ADAFLAGS_MAINT_APPEND := -gnatVcdefimnoprst -gnatafo

# #1068058 gtkada-canvas_view.adb: Assembler messages: branch out of range
ifeq ($(DEB_HOST_ARCH),armhf)
  ifeq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
    DEB_ADAFLAGS_MAINT_APPEND += -O1
  endif
endif

DEB_CFLAGS_MAINT_APPEND := -Wall -Wextra -fstack-check
# debian/lto-failure-log completes the description in #1015492.
DEB_BUILD_MAINT_OPTIONS := hardening=+all optimize=-lto
DEB_LDFLAGS_MAINT_APPEND := \
  -Wl,--no-allow-shlib-undefined \
  -Wl,--no-copy-dt-needed-entries \
  -Wl,--no-undefined
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/buildopts.mk
include /usr/share/ada/packaging.mk

# TODO: why is AC_CONFIG_MACRO_DIR([m4]) not sufficient?
export ACLOCAL := aclocal -Im4

override_dh_auto_configure:
 # Requires pkg-config package.
	dh_auto_configure -- \
	  --enable-shared \
	  --disable-static-pic \
	  --includedir=$${prefix}/share/ada/adainclude \
	  CC=gcc-$(DEB_GNAT_VERSION) \
	  PACKAGE_VERSION=$(gtkada_SO_VERSION)

# Work around because gtk.gpr is not generated.
export GTK_PREFIX := /usr

override_dh_auto_build-arch:
 # Instead of running default target all: static relocatable tools tests,
 # - write generated Ada sources (removed by the clean target)
 # - link tools dynamically
 # - run tests only if DEB_BUILD_OPTIONS does not contain nocheck.

 # We want one gprbuild main process, even if itself deals with
 # parallelism.  Hence, prefer MAKE to dh_auto_build (=make -jN).

 # When changing this, check that the library is only built twice.
	$(MAKE) generate src/gtkada-intl.adb PYTHON=python3
	gprconfig --batch $(GPRCONFIGFLAGS)
	gprbuild -m -p $(GPRBUILDFLAGS) src/gtkada.gpr -XLIBRARY_TYPE=static
	gprbuild -m -p ${GPRBUILDFLAGS} src/tools/tools.gpr

override_dh_auto_test-arch:
	gprbuild -m -p $(GPRBUILDFLAGS) testgtk/testgtk.gpr

override_dh_auto_install-arch:
	gprinstall src/tools/tools.gpr $(standard_GPRINSTALLFLAGS)
  # 20221214: gprinstall ignores the --no-project option.
  #   Work-around: install the (buggy) static project to the default
  #   location first, then overwrite it with the (desired) shared
  #   project (the locations match).
	gprinstall src/gtkada.gpr $(static_GPRINSTALLFLAGS) \
	  -XLIBRARY_TYPE=static
	gprinstall src/gtkada.gpr $(call shared_GPRINSTALLFLAGS,gtkada)

# Archive testgtk sources, so that the user does not need to
# uncompress them separately.
# Remove objects and executables.
testgtk_exclude := obj/* test_rtree testgtk testgtk.gpr \
  task_project/task_project.gpr
reproducible_tar_options := --owner=0 --group=0 --numeric-owner \
  --preserve-permissions
reproducible_gz_options  := --no-name

# TODO: add the reference manual when gnat-gps/gnatdoc is available
override_dh_auto_build-indep:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
  # We want one sphinx main process, even if itself deals with parallelism.
  # Hence, prefer MAKE to dh_auto_build (=make -jN).
	$(MAKE) -Cdocs/gtkada_ug latexpdf html \
	  $(addprefix SPHINXOPTS=-j,$(DEB_BUILD_OPTION_PARALLEL))

  # Exclude or transform options only affect following files.
	tar --create \
	  $(reproducible_tar_options) \
	  $(addprefix --exclude=testgtk/,$(testgtk_exclude)) testgtk \
	  --transform=s_debian_testgtk_ debian/testgtk.gpr \
	  | gzip $(reproducible_gz_options) > testgtk.tgz
endif

override_dh_auto_test-indep:

override_dh_auto_install-indep:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
# See debian/patches/gprinstall-no-indep-artifacts.diff.
	mkdir -p debian/tmp/usr/share/doc/gtkada/gtkada_ug
	cp --reflink=auto -aftdebian/tmp/usr/share/doc/gtkada/gtkada_ug \
	  docs/gtkada_ug/_build/html/* \
	  docs/gtkada_ug/_build/latex/GtkAda.pdf
endif
